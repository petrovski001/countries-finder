import './App.scss'
import InputSearch from './components/input-search/InputSearch';

function App() {

  return (
    <div className="App">
      <h1>Find your country</h1>
      <InputSearch />
    </div>
  )
}

export default App
