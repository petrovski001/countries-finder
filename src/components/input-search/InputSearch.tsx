import { ChangeEvent } from 'react';
import InputResults from '../input-results/InputResults';
import useCountries from '../../hooks/useCountries';
import './InputSearch.scss';

function InputSearch() {
  const [countries, searchTerm, setSearchTerm] = useCountries();

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const {value} = event.target
    setSearchTerm(value)
  }

  return (
    <div className='search'>
        <label htmlFor={'country'}>Search for a country</label>
        <input onChange={handleChange} name={'country'} value={searchTerm} id={'country'} type={'text'} />
        <InputResults countries={countries} />
    </div>
  )
}

export default InputSearch