import '../InputResults.scss'

interface ItemProps {
    country: any
}

export default function Item({country}: ItemProps) {
  const capital = country?.capital || '';
  const population = (country?.population as number).toLocaleString()
  return (
    <div className="item" key={country.area}>
        <div className="flag">
          <img src={country.flags.svg} alt={country.fifa} />
        </div>
        <div className="description">
          <div className="desc-name">
            <p>{country.translations.cym.common}</p>
          </div>
          <div className="desc-info">
            <small>{`capital: ${capital} | population: ${population}`}</small>
          </div>
        </div>
    </div>
  )
}
