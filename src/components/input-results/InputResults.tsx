import './InputResults.scss'
import Item from './item/Item'

interface InputResultsProps {
    countries: any[]
}

export default function InputResults({countries}: InputResultsProps) {

  const renderCountries = () => {
    return countries.map((country: any) => <Item country={country} />)
  }

  return <>
      {countries?.length > 0 && <div className="results">
        { renderCountries()}
      </div>}
    </>
}

