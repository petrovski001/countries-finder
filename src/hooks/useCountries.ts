import axios from "axios";
import { useEffect, useState } from "react";

export default function useCountries () {
  const [searchTerm, setSearchTerm] = useState<string>('')
  const [countries, setCountries] = useState<Array<object>>([])

  const fetchCountries = async () => {
    try {
      if(searchTerm) {
        const response = await axios.get(`https://restcountries.com/v3.1/name/${searchTerm}`)
        const countries = response.data
        setCountries(countries)
        console.log(countries)
      }
    } catch {
      setCountries([])
      console.error('ERROR FETCHING COUNTRIES')
    }
  };

  useEffect(() => {
    if(searchTerm == '') {
      setCountries([]);
    }
    fetchCountries()
  }, [searchTerm]);

  return [countries, searchTerm, setSearchTerm] as const
}
